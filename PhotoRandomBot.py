from time import sleep
import requests
import json

import telegram
from telegram.error import NetworkError, Unauthorized


def start_bot():
    """ Start the PhotoRandomBot """

    bot_token = 'TOKEN'
    bot = telegram.Bot(bot_token)

    up_date_id = None
    while True:
        try:
            up_date_id = resp_to_comm(bot, up_date_id)
        except NetworkError as e:
            sleep(1)
        except Unauthorized as e:
            print('The user has removed or blocked the bot')
            break


def show_menu(bot, chat_id):
    """ Help navigate """
    bot.send_message(chat_id, 'Click on /more ')


def resp_to_comm(bot, up_date_id=None):
    """ Response to commands """
    for new_data in bot.getUpdates(offset=up_date_id, timeout=10):
        if new_data.message is not None:

            try:
                print(new_data.message['chat']['first_name'] + ' : ' + new_data.message['text'])
            except:
                print('Неизвестная команда')

            chat_id = new_data.message['chat']['id']
            if new_data.message['text'] == '/more':
                send_file(bot, get_url_file(), chat_id)

        show_menu(bot, chat_id)
        up_date_id = new_data.update_id + 1

    return up_date_id


def get_url_file():
    """ Return URL photo """
    response = requests.get("https://random.dog/woof.json")
    parsed_string = json.loads(response.text)
    return parsed_string["url"]


def send_file(bot, url, chat_id):
    """ Send to file by URL """
    if url.split(".")[-1].lower() in "jpgpngjpeg":
        bot.sendPhoto(chat_id, url)
        return

    if url.split(".")[-1].lower() in "mp4":
        bot.send_video(chat_id, url)
        return

    if url.split(".")[-1].lower() in "gif":
        bot.send_animation(chat_id, url)
        return

    bot.send_message(chat_id, "Ops, I can't understand the file type. Try again! We will succeed!")


print('Start the BOT')
if __name__ == '__main__':
    start_bot()


